//Se encarga de inicializar eventos cuando lapágina es cargada.
addEventListener('load', inicializarEventos, false);

//Se encarga de agregar el evento onClik al botón con el id=boton1
function inicializarEventos() {
    var ob = document.getElementById('boton1');
    ob.addEventListener('click', presionBoton, false);
}

function presionBoton(e) {
    var dato = document.getElementById('dato');
    recuperarDato(dato.value);
}

var conexion;

function recuperarDato(dato) {
    conexion = new XMLHttpRequest();
    conexion.onreadystatechange = manejadorConexion;
    conexion.open('GET', 'servidor.php?dato=' + dato, true);
    conexion.send();
}

function manejadorConexion() {
    var info = document.getElementById('informacion');
    if (conexion.readyState == 4) {
        var json = JSON.parse(conexion.responseText);
        var info_recuperada = "Microproceador =" + json.microprocesador + "</br>";
        info_recuperada += "Memoria Ram ->" + json.memoria + "</br>";
        info_recuperada += "Capacidad de Disco ->" + json.discos + "</br>";
        //info_recuperada += "Capacidad de Disco 2 ->" + json.discos[1] + "</br>";
        info.innerHTML = info_recuperada;
    } else {
        info.innerHTML = "Cargando...";
    }
}